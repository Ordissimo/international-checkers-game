Les règles de bases:
    - Les pions avancent et mangent uniquement en diagonale
    - Quand un pion arrive sur la dernière ligne, il fait une promotion et devient une dame
    - Les pions n’avancent que d’une case à la fois
    - Les blancs commencent toujours

A) Ouverture de l’application
    - Une boite de dialogue apparait, elle permet de charger votre précédente partie, si une sauvegarde existe.
    - Le bouton «Fermer», ferme l’application et sauvegarde la partie en cours.
    - Les boutons «Nouvelle partie» et «Paramètres» permettent de mettre en place des options
    - Le bouton «Aide» ouvre ce texte dans une boite de dialogue

B) «Nouvelle Partie»
    Cette boite de dialogue propose plusieurs choix :
       - Le niveau de difficulté de l’ordinateur
       - Qui de l'ordinateur ou du joueur commence
       - Les choix des règles appliquées en fonction du pays: 
            a) Pays-Bas, la taille d'une ligne est 10, les cases blanches sont les cases utilisées, le pion n’est pas dans le coin inférieur droit, les prises sont obligatoires, les pions peuvent manger en arrière, les pions peuvent manger une dame, la dame peut faire de longs déplacements, les pions ne peuvent pas faire une promotion au cours d’une rafle.
            b) France, la taille d’une ligne est 10, les cases blanches sont les cases utilisées, le pion n’est pas dans le coin inférieur droit,  les prises sont obligatoires, les pions peuvent manger en arrière, les pions peuvent manger une dame, la dame peut faire de longs déplacements, les pions ne peuvent pas faire une promotion au cours d’une rafle.
            c) Espagne, la taille d’une ligne est 8, les cases blanches sont les cases utilisées, le pion est dans le coin inférieur droit, les prises sont obligatoires, les pions ne peuvent pas manger en arrière, les pions peuvent manger une dame, la dame peut faire de longs déplacements, les pions ne peuvent pas faire une promotion au cours d’une rafle.
            d) Italie, la taille d’une ligne est 8, les cases noires sont les cases utilisées, le pion est dans le coin inférieur droit, les prises sont obligatoires, les pions ne peuvent pas manger en arrière, les pions ne peuvent pas manger une dame, la dame peut faire de longs déplacements, les pions peuvent faire une promotion au cours d’une rafle.
            e) Angleterre, la taille d’une ligne est 8, les cases noires sont les cases utilisées, le pion n’est pas dans le coin inférieur droit, les prises sont obligatoires, les pions ne peuvent pas manger en arrière, les pions peuvent manger une dame, la dame se déplace d’une case seulement, les pions ne peuvent pas faire une promotion au cours d’une rafle

C) «Paramètres»
    Cette boite de dialogue propose plusieurs choix :
       - Le niveau de difficulté de l’ordinateur
       - Qui de l'ordinateur ou du joueur commence
       - Les possibilités de jouer aux dames en composant les règles que vous souhaitez, si votre pays n'est pas listé dans la boite de dialogue «Nouvelle Partie»:
            a) Combien de pions voulez-vous par ligne?
            b) Sur quelles cases vous ne voulez pas jouer?
            c) Est-ce que votre pion doit être dans le coin inférieur droit?
            d) Les prises sont-elles obligatoires?
            e) Les pions peuvent-ils manger en arrière?
            f) Les pions ont-ils le droit de manger une dame?
            g) Une dame doit-elle se déplacer uniquement d’une case?
            f) Les pions peuvent-ils faire promotion au cours d’une rafle?


D) Le plateau
    Pour jouer un coup, il faut cliquer sur le pion que l’on souhaite déplacer (sa case se colorie en rouge) et ensuite cliquer sur la case où l’on veut aller. Si au premier clic, le case ne devient pas rouge c’est que le jeu n’autorise pas le coup.
Lorsqu’un coup est obligatoire, tous les pions qui peuvent se déplacer ont leurs cases qui se colorient en vert.
Au dessus du plateau de jeu, des messages s’affichent pour vous guider durant toute la partie.

E) «Historique»
    Cette partie utilise la notation Manoury et permet de refaire et d’analyser des parties.
